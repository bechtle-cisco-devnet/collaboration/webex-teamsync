﻿param (
    [string]$groupName,
    [string]$outputFile = 'members.csv'
)

$groups = Get-ADGroup -Filter 'Name -like "$groupName"'

$groups | ForEach-Object {
    $members = ''
    $groupname = $_.Name

    $members = Get-ADGroupMember -Identity  $groupname -Recursive | Get-ADUser -Properties Mail | Select-Object Mail
    $members | Add-Member -MemberType NoteProperty -Name Group -Value $groupname
    $members | Export-Csv -Path $outputfile -NoTypeInformation -Append -Encoding UTF8
}