﻿<#
> Run 'Get-Help .\Sync-Csv2Webex.ps1 -full' to access the content below.

.SYNOPSIS
A script for synchronizing the members of a Webex room or teams against a given CSV file.

.DESCRIPTION
After providing a CSV filename and a valid Webex Access Token, room and team memberships will synchronize against two CSV files.
Both CSV files are optional. Filename "room.csv" is used to synchronize members or a single room. Filename "teams.csv" is used to
synchronize team memberships.

The CSV column structure for both files can be found in templates 'room.csv.tpl' and 'teams.csv.tmpl'. The first line of a file will
be removed by default. You can change this behaviour with parameter -removeheader. 

The structure for 'room.csv' is simply:

| e-mail address |

And for 'teams.csv':

| e-mail address | teamname |

IMPORTANT: To protect certain accounts from getting removed of a team, add the according e-mail adresses to 'protected.csv'.
This is also necessary for your Webex Access Token user or bot.

To allow the script to access the Webex data, you need to enter a Webex Access Token when asked to. Get the token from developer.webex.com, 
specifically from 'https://developer.webex.com/docs/api/getting-started'.

Copy the token from the Box "Your Personal Access Token". You can use the copy button next to the box to put the token into your clipboard.
ATTENTION: These tokens only have a short lifetime.

To run the script more frequently and with a permanent token, you can create a bot under developer.webex.com and use the bot token. In this
case you'll also need to add the bot account to every room or team (with team moderator rights) to give it the necessary access rights.

.LINK
https://gitlab.com/bechtle-cisco-devnet/collaboration/webex-teamsync

.EXAMPLE
PS> ./Sync-Csv2Webex.ps1 -Token _token_

.EXAMPLE
PS> ./Sync-Csv2Webex.ps1 -Token _token_ -RoomId _roomid_ -RemoveHeader $true

.PARAMETER Token
REQUIRED - A valid Webex Access Token

.PARAMETER RoomId
REQUIRED FOR ROOM SYNC - Webex API RoomID for the room you want to synchronize. Defaults to "".

.PARAMETER RemoveHeader
OPTIONAL - Option to remove the CSV header row. Defaults to $false. 

.PARAMETER RoomCsv
OPTIONAL - Name of the room CSV file. Defaults to "room.csv".

.PARAMETER TeamCsv
OPTIONAL - Name of the team CSV file. Defaults to "teams.csv".

.PARAMETER Delimiter
OPTIONAL - Delimiter for the CSV format. Needs to be identical for all CSV files.

.OUTPUTS
> Logging output to /logs.

.NOTES
File Name      : Sync-Csv2Webex.ps1
Author         : Christian Drefke (christian.drefke@bechtle.com)
Prerequisite   : PowerShell V5.1 (tested with 5.1, might work with lower versions)

Copyright 2022 - Christian Drefke

REQUIREMENTS

TODO * PowerShell version

INSTALL

The RSAT Modules are necessary to export AD information.

´Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online´

TROUBLESHOOTING

TODO * TLS 1.2

To see the Debug output, switch Debug preference from 'SilentlyContinue' to 'Continue':

    PS C:\> $DebugPreference = "Continue"
#>

param (
    [string]$Token = $env:WebexAPIToken,
    [string]$RoomId = "",
    [string]$TeamName = "",
    [bool]$RemoveHeader = $False,
    [string]$RoomCsv = "room.csv",
    [string]$TeamsCsv = "teams.csv",
    [string]$Delimiter = ","
)

# Activate Informational Console Output
$InformationPreference = "Continue";

function Get-Directory($initdir = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $dirbox = New-Object System.Windows.Forms.FolderBrowserDialog
    $dirbox.Description = "Select a folder"
    $dirbox.rootfolder = "MyComputer"
    $dirbox.SelectedPath = $initdir

    Write-Log "Select your output directory from the popup dialog." "INFO"
    if ($dirbox.ShowDialog() -eq "OK") {
        $directory += $dirbox.SelectedPath
    }
    return $directory
}

class Webex {
    [string] $Protocol = 'https'
    [string] $Hostname = 'webexapis.com'
    [string] $Port = '443'
    [string] $ApiVersion = '1'
    hidden [string] $Token

    Webex() {}

    Webex(
        [string]$Token) {
        $this.Token = $Token
    }

    [PSCustomObject] AddMembership([string] $RoomId, [string] $personEmail) {
        Write-Log "Adding new member '$personEmail' to room id '$RoomId'." "INFO"

        $endpoint = "/memberships"
        $params = $this.GetParams($endpoint, "POST")
        $body = @{
            roomId = $RoomId
            personEmail = $personEmail
        }
        $jsonbody = $body | ConvertTo-Json

        $params.ContentType = "application/json"
        $params.Body = $jsonbody

        $result = $this.SendRequest($params, $true)

        Write-Log "Room membership $($result.id) created." "INFO"
        
        return $result
    }

    [PSCustomObject] AddTeamMembership([string] $teamId, [string] $personEmail) {
        Write-Log "Adding teammember '$personEmail' to team '$teamId'." "INFO"

        $endpoint = "/team/memberships"
        $params = $this.GetParams($endpoint, "POST")
        $body = @{
            teamId = $teamId
            personEmail = $personEmail
        }
        $jsonbody = $body | ConvertTo-Json

        $params.ContentType = "application/json"
        $params.Body = $jsonbody

        $result = $this.SendRequest($params, $true)

        Write-Log "Team membership $($result.id) created." "INFO"
        
        return $result
    }

    [void] GetAttachment($fileurl, $directory) {

        $endpoint = ""
        $params = $this.GetParams($endpoint, "HEAD")
        $params.Uri = $fileurl
        $result = $this.SendRequest($params, $false)

        #Write-Debug $result["Content-Disposition"]
        $filename = [regex]::matches($result["Content-Disposition"], '(?<=\").+(?=\")').value
        #Write-Debug "Filename: $filename"

        $params.Method = "GET"
        $params.OutFile = $directory + "/" + $filename

        $this.SendApiRequest($params)
    }

    [PSCustomObject] GetMe() {
        Write-Log "Getting information about Me." "INFO"

        $endpoint = "/people/me"
        $method = "GET"
        $params = $this.GetParams($endpoint, $method)

        return $this.SendRequest($params, $false)
    }

    [PSCustomObject] GetMemberships($RoomId) {
        Write-Log "Getting memberships for room '$RoomId'" "INFO"

        $endpoint = "/memberships/?max=1000&roomId=" + $RoomId
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of room members in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] GetMessages($room) {
        Write-Log "Getting all room messages. This can take a moment for large rooms." "INFO"
        $endpoint = "/messages/?RoomId=$room&max=1000"

        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)
        $m = $result.items | Measure-Object

        Write-Log "Number of messages in room: $($m.Count)" "INFO"
        
        return $result
    }

    [hashtable] GetParams([string] $endpoint, [string] $method) {
        $url = $this.Protocol + "://" + $this.Hostname + ":" + $this.Port + "/v" + $this.ApiVersion + $endpoint
        $headers = @{ "Authorization" = "Bearer $($this.token)" }

        $params = @{
            Uri     = $url
            Headers = $headers
            Method  = $method
        }
        return $params
    }

    [PSCustomObject] GetRooms() {
        Write-Log "Getting list of rooms." "INFO"

        $endpoint = "/rooms/?sortBy=lastactivity&max=1000"
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of rooms in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] GetTeamByName([string] $name) {
        # Get a team Id by searching for the team name.
        Write-Log "Getting ID of team '$name'" "INFO"

        $teams = $this.GetTeams()

        # DEBUG
        # Write-Host ($teams.items | Format-Table | Out-String)

        $filtered = $teams.items | Where-Object {$_.name -eq $teamName}
        if ($filtered) {
            $result = $filtered.id
        } else {
            Write-Log "Team '$name' not found." "INFO"
            $result = $null
        }
        return $result
    }

    [PSCustomObject] GetTeamMemberships($teamId) {
        Write-Log "Getting team memberships." "INFO"

        $endpoint = "/team/memberships/?max=1000&teamId=" + $teamId
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of teammembers in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] GetTeams() {
        Write-Log "Getting list of teams." "INFO"

        $endpoint = "/teams/?max=1000"
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of teams in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] RemoveMembership([string] $MembershipId) {
        Write-Host "Removing room membership '$MembershipId'." "INFO"

        $endpoint = "/memberships/" + $MembershipId
        $params = $this.GetParams($endpoint, "DELETE")
        $result = $this.sendRequest($params, $true)

        Write-Log "Room membership '$MembershipId' deleted." "INFO"
        
        return $result
    }

    [PSCustomObject] RemoveTeamMembership([string] $MembershipId) {
        Write-Log "Removing team membership '$MembershipId'." "INFO"

        $endpoint = "/team/memberships/" + $MembershipId
        $params = $this.GetParams($endpoint, "DELETE")
        $result = $this.sendRequest($params, $true)

        Write-Log "Team membership $MembershipId deleted." "INFO"

        return $result
    }

    [PSCustomObject] SendRequest([hashtable] $params, [bool] $paged = $false) {
        $response = $this.SendApiRequest($params)
        if ($params.Method -eq "HEAD") {
            $result = $response.Headers
            return $result
        }
        $result = $response.Content | ConvertFrom-Json

        if ($paged -eq $true) {
            $linkheader = $response.Headers.Link
            while ($linkheader -match '<(?<Link>.+)>; rel="next"') {
                $params.Uri = $Matches.Link
                $response = $this.SendApiRequest($params)
                $temp = $response.Content | ConvertFrom-Json
                #$m = $temp.items | measure
                #Write-Debug "Number of temp items: $($m.Count)"
                $result.items += $temp.items

                $linkheader = $response.Headers.Link
            }
            return $result
        }
        else {
            return $result
        }
    }

    [Object] SendApiRequest($params) {
        try {
            Write-Log "Sending Web Request with Params: uri=$($params.Uri) method=$($params.Method)"
            if ($params.Body) {
                Write-Log "Sending POST data: $($params.Body)"
            }
            $response = Invoke-WebRequest @params

            # Little hack cause $response will be empty if -OutFile is in Webrequest
            if ($params.ContainsKey("OutFile")) { response = @{} }

            return $response
        }
        catch {
            $Statuscode = $_.Exception.Response.StatusCode.value__
            $Message = $_.Exception.Message
            Write-Log "$Statuscode - $Message" "DEBUG"

            Switch ( $Statuscode) {
                "400" { Write-Log "400 - Request Failed." "CRITICAL" }
                "401" { Write-Log "401 - Authentication failed. Provide a valid token." "CRITICAL" }
                "404" { Write-Log "404 - Endpoint Url $($params.Uri) does not exist." "ERROR" }
                "409" { Write-Log "409 - There was a conflict sending the request. The target might already exist." "ERROR" }
                "423" {
                    $waitfor = $_.Exception.Response.Headers["Retry-After"]
                    Write-Log "Resource is temporarily unavailable, will have to wait for $waitfor seconds." "INFO"
                    Start-Sleep -s $waitfor
                    $this.SendApiRequest($params)
                }
                "429" {
                    $waitfor = $_.Exception.Response.Headers["Retry-After"]
                    Write-Log "Too many requests, will have to wait for $waitfor seconds." "INFO"
                    Start-Sleep -s $waitfor
                    $this.SendApiRequest($params)
                }
                "500" { Write-Log "Target system is experiencing an error. Try again later." "ERROR" }
                "503" { Write-Log "Target system is overloaded. Try again later." "ERROR" }
            }
            Write-Log ("Web Response Status Code: $statuscode")
            return @{"statuscode" = $statuscode }
        }
    }
}

function getCsvFile {
    $lang = Get-WinSystemLocale

    if ($lang.Name -eq "de-DE") {
        $filename = Read-Host -Prompt "Bitte geben Sie den Namen oder Pfad der CSV Importdatei an."
    }
    else {
        $filename = Read-Host -Prompt "Please provide the name or path of the CSV import file."
    }
    return $filename
}

function getToken {
    $lang = Get-WinSystemLocale

    if ($lang.Name -eq "de-DE") {
        $token = Read-Host -Prompt "Fügen Sie hier Ihren Persönlichen Token von https://developer.webex.com/docs/api/getting-started ein"
    }
    else {
        $token = Read-Host -Prompt "Paste your Personal Access Token from https://developer.webex.com/docs/api/getting-started"
    }
    return $token
}

function Remove-FirstLine-From-File($FilePath) {
    Write-Log "Removing first line from file: '$FilePath'." "INFO"

    $Content = Get-Content -Path $FilePath
    $Content = $Content[1..($Content.Count -1)]
    $Content | Out-File -FilePath $FilePath
}

function Sync-LeftToRight {

    param (
        [ValidateNotNullOrEmpty()]
        [System.Array]$membersSource,

        [ValidateNotNullOrEmpty()]
        [System.Array] $membersTarget,

        [string] $roomOrTeam = "room"
    )
    $membersCompared = Compare-Object -ReferenceObject $membersSource.email -DifferenceObject $membersTarget.personEmail | Where-Object SideIndicator -eq "<="
    # DEBUG
    # Write-Host ($membersCompared.InputObject | Format-Table | Out-String)

    Write-Log "Found $($membersCompared.Count) accounts for ADD jobs." "INFO"

    foreach ($Member in $membersCompared) {
        if ($roomOrTeam -eq "room") {
            $Member = $Member.InputObject
        
            $client.AddMembership($RoomId, $Member)
        }
        elseif ($roomOrTeam -eq "team") {
            $client.AddTeamMembership($teamId, $Member.InputObject)
        }
        else {
            Write-Log "Parameter 'roomOrTeam' must be either 'room' or 'team'." "ERROR"
        }
    }
}

function Sync-Room {

    Param (
        [ValidateNotNullOrEmpty()]
        [string] $CsvFile,
        
        [ValidateNotNullOrEmpty()]
        [string] $RoomId,

        [bool] $RemoveHeader = $False,
        [string] $Delimiter = ","
    )
    Write-Log "Synchronizing room CSV '$CsvFile'." "INFO"
    
    # Initiate the Webex API Client
    $client = [Webex]::new($token)
    
    # Import Active Directory Member data after deleting the header row
    $Header = 'email'
    
    if ($RemoveHeader -eq $True) {
        Remove-FirstLine-From-File($CsvFile)
    }
    $membersImport = Import-Csv -Path $CsvFile -Delimiter $Delimiter -Header $Header
    Write-Log "$($membersImport.Count) entries found in CSV file."

    # DEBUG
    # Write-Host ($membersImport | Format-Table | Out-String)

    # Only valid for named imports
    # $membersSource = $membersImport | Where-Object name -eq $_ | Select-Object email
    $membersSource = $membersImport | Select-Object email
    # DEBUG
    # $membersSource | Get-Member

    # DEBUG
    # Write-Host ($teamsReference | Format-Table | Out-String)

    Write-Log "Processing Rooom ID '$RoomId'" "INFO"

    $membersTarget = $client.GetMemberships($RoomId)
    $membersTarget = $membersTarget.items | Select-Object -Property id, personEmail

    # DEBUG
    # $membersTarget | Get-Member

    ### Import Users who are not yet members of the Room. ###
    Sync-LeftToRight $membersSource $membersTarget
    ###########################################################

    ### Delete Memberships of users who are in the Source List anymore. ###
    Sync-RightToLeft $membersSource $membersTarget
    ###########################################################
}
function Sync-RightToLeft {

    Param (
        [ValidateNotNullOrEmpty()]
        [System.Array]$membersSource,

        [ValidateNotNullOrEmpty()]
        [System.Array] $membersTarget,

        [string] $roomOrTeam = "room"
    )
    # Add protected accounts to the Source Liste
    try {
        $protected = Import-Csv -Path .\protected.csv -Delimiter "," -Header 'email'
        $membersSource = $membersSource + $protected        
    }
    catch {
        Write-Log "No file '.\protected.csv'. Processing withough protected accounts." "INFO"
    }

    $membersCompared = Compare-Object -ReferenceObject $membersSource.email -DifferenceObject $membersTarget.personEmail | Where-Object SideIndicator -eq "=>"
    # DEBUG
    # Write-Host ($membersCompared.InputObject | Format-Table | Out-String)
    
    Write-Log "Found $($members.Compared.Count) accounts for REMOVE jobs." "INFO"

    foreach ($member in $membersCompared) {
        if ($roomOrTeam -eq "room") {
            $membership = $membersTarget | Where-Object personEmail -eq $member.InputObject
            # DEBUG
            # Write-Host ($membership.id)
    
            $client.RemoveMembership($membership.id)
        }
        elseif ($roomOrTeam -eq "team") {
            $membership = $membersTarget | Where-Object personEmail -eq $member.InputObject
            # DEBUG
            # Write-Host ($membership.id)
    
            $client.RemoveTeamMembership($membership.id)
        }
        else {
            Write-Log "Parameter 'roomOrTeam' must be either 'room' or 'team'." "CRITICAL"
        }
    }
}

function Sync-Teams {

    Param (
        [ValidateNotNullOrEmpty()]
        [string] $CsvFile,

        [string] $TeamName = "",
        [bool] $RemoveHeader = $False,
        [string] $Delimiter = ","
    )
    Write-Log "Synchronizing team CSV '$CsvFile'." "INFO"

    # Initiate the Webex API Client
    $client = [Webex]::new($token)

    # Import Active Directory Member data after deleting the header row
    $Header = 'email', 'name'
    
    if ($RemoveHeader -eq $True) {
        Remove-FirstLine-From-File($CsvFile)
    }
    $membersImport = Import-Csv -Path $CsvFile -Delimiter $Delimiter -Header $Header
    Write-Log "$($membersImport.Count) entries found in CSV file."
    # DEBUG
    Write-Host ($membersImport | Format-Table | Out-String)

    $teamsReference = $membersImport.name | Select-Object -Unique
    # DEBUG
    # Write-Host ($teamsReference | Format-Table | Out-String)
    
    if ($teamName) {
        # Get the Team ID if name was given.
        $teamId = $client.GetTeamByName($teamName)
    }

    $teams = $client.GetTeams()
    
    if ($teams.items.Count -eq 0) {
        Write-Log "No access to any teams. Skipping team synch."
        return
    }
    
    $teamsCompared = Compare-Object -ReferenceObject $teamsReference -DifferenceObject $teams.items.name -IncludeEqual | Where-Object SideIndicator -eq "=="
    # DEBUG
    # Write-Host ($teamsCompared.InputObject | Format-Table | Out-String)
    
    $teamsCompared.InputObject | ForEach-Object {
        Write-Log "Synchronizing team '$_'." "INFO"

        $teamId = $teams.items | Where-Object name -eq $_
        $teamId = $teamId.id
        
        # DEBUG
        # Write-Host $teamId

        $membersSource = $membersImport | Where-Object name -eq $_ | Select-Object email 
        # DEBUG
        # $membersSource | Format-Table | Out-String
        
        $membersTarget = $client.GetTeamMemberships($teamId)
        $membersTarget = $membersTarget.items | Select-Object -Property id, personEmail

        # DEBUG
        # $membersTarget | Get-Member
        
        ### Import Users who are not yet members of the Team. ###
        Sync-LeftToRight $membersSource $membersTarget "team"
        ###########################################################

        ### Delete Memberships of users who are in the Source List anymore. ###
        Sync-RightToLeft $membersSource $membersTarget "team"
        ###########################################################
    }
}

function Write-Log {

    Param (
        [string] $Message,
        [string] $Level = "DEBUG"
    )

    $LoggingPath = ".\Logs"

    if (!(Test-Path $LoggingPath)) {
        New-Item -Path ".\" -Name "Logs" -ItemType "directory"
    }

    $FileName = "log_$(Get-Date -Format "yyyyMMdd").txt"
    $FilePath = ".\Logs\$FileName"
    if (!(Test-Path $FilePath)) {
        New-Item -Path ".\Logs" -Name $filename -ItemType "file"
    }

    $TimeStamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    $LogEntry = $TimeStamp + " " + $Level + " " + $Message
    
    Add-Content ".\Logs\$FileName" -value $LogEntry

    Switch ( $Level ) {
        "DEBUG" { Write-Debug ($LogEntry) }
        "INFO" { Write-Information ($LogEntry) }
        "WARNING" { Write-Warning ($LogEntry) }
        "ERROR" { Write-Error ($LogEntry) }
        "CRITICAL" { Throw $LogEntry }
    }
}

Invoke-Command -ScriptBlock {
    Write-Log "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    $PSDefaultParameterValues["Export-CSV:Encoding"] = "UTF8"
    
    # DEBUG AND DEV ONLY
    # $token = ""
    # $teamId = ""
    # $teamId = ""
    # $RoomId = ""
    
    # Ask the user for a valid Webex token if not provided through parameters.
    if ([string]::IsNullOrEmpty($token)) {
        $token = getToken
    }
    # DEBUG
    # Write-Debug "Token: '$token'"

    # Ask the user for the name of the CSV file if not provided through parameters.
    # if ([string]::IsNullOrEmpty($RoomCsv)) {
    #        $CsvFile = getCsvFile
    #        if ([string]::IsNullOrEmpty($CsvFile)) {
    #            Write-Log "CSV filename cannot be empty." "CRITICAL"
    #         }
    #     }
    # else {
    #     $CsvFile = $RoomCsv
    # }

    if ((Test-Path -Path $RoomCsv -PathType Leaf) -eq $True) {
        if ($RoomId) {
            Sync-Room -CsvFile $RoomCsv -RoomId $RoomId -RemoveHeader $RemoveHeader
        }
        else {
            Write-Log "CSV file '$RoomCsv' found but Room ID is missing. Skipping room synch." "WARNING"
        }
    }
    else {
        Write-Log "CSV file '$RoomCsv' does not exist. Skipping room synch." "INFO"
    }

    if ((Test-Path -Path $TeamsCsv -PathType Leaf) -eq $True) {
        Sync-Teams -CsvFile $TeamsCsv -TeamName $TeamName -RemoveHeader $RemoveHeader
    }
    else {
        Write-Log "CSV file '$RoomCsv' does not exist. Skipping team synch." "INFO"
    }
}